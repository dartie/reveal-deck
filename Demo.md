---
title:
- AI Playground
author:
- Dario Necco
theme:
- Test
date:
- March 22, 2020
---


---#
# What's AI?

- Artificial Intelligence- The ability of machine to think and behave like humans.
- How does the machine learn on its own? - That is called Machine Learning. ML is the study of computer algorithms that improve automatically with experience.
- Just like humans learn with experience - Machines also learn with experience!
- Examples of common AI? Alexa, Siri, Google Home, Self Driving Cars, Robots etc.


---##
# What's out there?

![Verticles](img/Untitled.png)


---#
# How do computers make decisions?

- Conditional statements are used to perform different actions based on different conditions.
- In many programming languages, decisions (also called conditionals) take the form of an if-then construct. They start with a condition, which is then evaluated as either True or False.

---##
# How do computers make decisions?

![Flow chart](img/Untitled 1.png){ width=250px }

---#
# Let's Build that

``` C
int main() {
    int i = 0;
    int a = 2;

    if (int b) {
        return 0;
    }

    return 1;
}
```

