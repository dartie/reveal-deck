# Reveal Deck

Reveal-deck converts a markdown file to reveal.js slides. It requires a custom syntax only for handling:

* Vertical slides (https://revealjs.com/vertical-slides)
    
    `---#` means Horizontal slide
    
    `---##` means child Vertical slide

* Syntax highlighting focus / animation for block code (https://revealjs.com/code/)
    ```
    ```go 
    {data-line-numbers="1,2|5-6"}
    
    ...
    ```

    where:

    * `,` comma adds more lines to highlight
    * `|` char means it highlights on next shift (arrow down)
    * `-` dash specifies a range of lines to highlight 
    
