import os
import json
import jinja2

    
css_template = """
/* {{ type|title }} */
.admonition.{{ type }} {
  border-bottom-color: {{ color }};
  border-left-color: {{ color }};
  border-right-color: {{ color }};
  border-top-color: {{ color }};
}

.{{ type }} > .admonition-title {
  background-color: {{ color_opaque }};
  border-bottom-color: {{ color }};
  border-left-color: {{ color }};
  border-right-color: {{ color }};
  border-top-color: {{ color }};
}

.{{ type }} > .admonition-title::before {
  background-color: {{ background_color }};
  -webkit-mask-image: var({{ icon }});
  mask-image: var({{ icon }});
  -webkit-mask-repeat: no-repeat;
  mask-repeat: no-repeat;
  -webkit-mask-size: contain;
  mask-size: contain;
}
"""


def main():
    settings_file = os.path.join(os.path.dirname(__file__), "admonitions.json")
    with open(settings_file, "r") as read_settings:
        settings_content = read_settings.read()
    settings = json.loads(settings_content)

    css_admonitions = []
    for type, type_properties in settings.items():
        background_color = type_properties["background_color"]
        icon = type_properties["icon"]
        color = type_properties["color"]
        color_opaque = type_properties["color"].replace(")", ",.1)")

        template_env = jinja2.Environment(trim_blocks=True, lstrip_blocks=True, autoescape=True, extensions=['jinja2.ext.loopcontrols']).from_string(css_template) 
        result = template_env.render(type=type, background_color=background_color, icon=icon, color=color, color_opaque=color_opaque)  # list of variable=value to pass to the template file to call
        css_admonitions.append(result)
        css_admonitions.append("\n")
    
    output_file = os.path.join(os.path.dirname(__file__), "admonitions.css")
    with open(output_file, "w") as write_output:
        write_output.writelines(css_admonitions)


if __name__ == "__main__":
    main()

